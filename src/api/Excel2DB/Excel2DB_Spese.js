var logger = require("./../../logger.js");
var data = '';


module.exports = {

  InserisciSpesa_Log: function(req, res, mongoHandler) {
    data = "";

    logger.log("[Spese] [Inserimento]");

    req.on('data', function (chunk) {
         if(chunk !== undefined) {
           data += chunk;
         }
       }).on('end', function() {
         var jsResult;

         try {
           jsResult = JSON.parse(data);
         } catch (err) {
           res.writeHead(500, {});
           res.end("RESULT: KO -" + err) ;
           logger.log(err);
           return;
         }


       mongoHandler.collection('Spese').findOne({username: jsResult.username, Time: jsResult.Time, Quota: jsResult.Quota, Tipo: jsResult.Tipo, Descrizione: jsResult.Descrizione }, function(err, result) {
         if(result == null && err == null) {
             mongoHandler.collection('Spese').insert(jsResult, function(err, result) {
                   if(err != null) {
                     res.writeHead(500, {});
                     res.end("RESULT: KO") ;
                   } else {
                     logger.log("[Spese] Record inserito all'interno del DB");
                     res.end("OK");
                   }

                   return;
             });
         }
         else {
             logger.log("[Spese] Record già presente all'interno del DB");
             res.writeHead(500, {});
             res.end("RESULT: KO - Record già presente all'interno del DB") ;
             return;
         }
       });
     });


  },

  EliminaSpesa_Log: function(req, res, mongoHandler) {
    data = "";

    logger.log("[Spese] [Eliminazione]")

    req.on('data', function (chunk) {
         if(chunk !== undefined) {
           data += chunk;
         }
         }).on('end', function() {
       var jsResult;

       try {
         jsResult = JSON.parse(data);
       } catch (err) {
         res.writeHead(500, {});
         res.end("RESULT: KO -" + err) ;
         logger.log(err);
         return;
       }

       mongoHandler.collection('Spese').findOne({username: jsResult.username, Time: jsResult.Time, Quota: jsResult.Quota, Tipo: jsResult.Tipo, Descrizione: jsResult.Descrizione }, function(err, result) {
         if(result == null && err == null) {
             logger.log("[Spese] Record non presente all'interno del DB");
             res.writeHead(500, {});
             res.end("RESULT: KO - Record non presente all'interno del DB") ;
             return;
         }
         else {
             mongoHandler.collection('Spese').deleteOne({username: jsResult.username, Time: jsResult.Time, Quota: jsResult.Quota, Tipo: jsResult.Tipo, Descrizione: jsResult.Descrizione }, function(err, result) {
                 if(err != null) {
                   res.writeHead(500, {});
                   res.end("RESULT: KO") ;
                 } else {
                   res.end("OK");
                 }
             });
         }
       });
     });

  },

  InserisciMensilita_Log: function(req, res, mongoHandler) {
    data = "";

    logger.log("[Mensilita] [Inserimento]");

    req.on('data', function (chunk) {
         if(chunk !== undefined) {
           data += chunk;
         }
         }).on('end', function() {
         var jsResult;

         try {
           jsResult = JSON.parse(data);
         } catch (err) {
           res.writeHead(500, {});
           res.end("RESULT: KO -" + err) ;
           logger.log(err);
           return;
         }


       mongoHandler.collection('Mensilita').findOne({username: jsResult.username, Mese: jsResult.Mese }, function(err, result) {
         if(result == null && err == null) {
             mongoHandler.collection('Mensilita').insert(jsResult, function(err, result) {
                   if(err != null) {
                     res.writeHead(500, {});
                     res.end("RESULT: KO") ;
                   } else {
                     logger.log("[Mensilita] Record inserito all'interno del DB");
                     res.end("OK");
                   }

                   return;
             });
         }
         else {
             mongoHandler.collection('Mensilita').updateOne({ _id: result._id }, jsResult, function(err, result) {
                 if(err != null) {
                   res.writeHead(500, {});
                   res.end("RESULT: KO") ;
                 } else {
                   res.end("OK - Aggiornamento");
                 }
             });
         }
       });
     });
  },

  EliminaMensilita_Log: function(req, res, mongoHandler) {
    data = "";

    logger.log("[Mensilita] [Eliminazione]")

    req.on('data', function (chunk) {
         if(chunk !== undefined) {
           data += chunk;
         }
         }).on('end', function() {
       var jsResult;

       try {
         jsResult = JSON.parse(data);
       } catch (err) {
         res.writeHead(500, {});
         res.end("RESULT: KO -" + err) ;
         logger.log(err);
         return;
       }

       mongoHandler.collection('Mensilita').findOne({username: jsResult.username, Mese: jsResult.Mese }, function(err, result) {
         if(result == null && err == null) {
             logger.log("[Mensilita] Record non presente all'interno del DB");
             res.writeHead(500, {});
             res.end("RESULT: KO - Record non presente all'interno del DB") ;
             return;
         }
         else {
             mongoHandler.collection('Mensilita').deleteOne({username: jsResult.username, Mese: jsResult.Mese }, function(err, result) {
                 if(err != null) {
                   res.writeHead(500, {});
                   res.end("RESULT: KO") ;
                 } else {
                   res.end("OK");
                 }
             });
         }
       });
     });

  },
}
