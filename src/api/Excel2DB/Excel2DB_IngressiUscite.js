var logger = require("./../../logger.js");
var data = '';

module.exports = {

    Login: function(req, res, mongoHandler) {
        logger.log("[Gestione Ingressi] Login")
        data = '';

        req.on('data', function (chunk) {
           if(chunk !== undefined) {
             data += chunk;
           }
        }).on('end', function() {
         var jsResult;

         try {
           jsResult = JSON.parse(data);
         } catch (err) {
           res.writeHead(500, {});
           res.end("RESULT: KO -" + err) ;
           logger.log(err);
           return;
         }
         mongoHandler.collection('Utenti').find({username: jsResult.username, password: jsResult.password}).toArray(function(err, result) {
                   if(err != null) {
                     res.writeHead(500, {});
                     res.end("RESULT: KO") ;
                   } else {
                      if(result.length === 0) {
                          res.end("false");
                      } else {
                          res.end("true");
                      }
                   }
            });
        });
    },


    Register: function(req, res, mongoHandler) {
        logger.log("[Gestione Ingressi] Register")
        data = '';

        req.on('data', function (chunk) {
           if(chunk !== undefined) {
             data += chunk;
           }
        }).on('end', function() {
         var jsResult;

         try {
           jsResult = JSON.parse(data);
         } catch (err) {
           res.writeHead(500, {});
           res.end("RESULT: KO -" + err) ;
           logger.log(err);
           return;
         }
         mongoHandler.collection('Utenti').find({username: jsResult.username}).toArray(function(err, result) {
                   if(err != null) {
                     res.writeHead(500, {});
                     res.end("RESULT: KO") ;
                   } else {
                      if(result.length > 0) {
                        res.writeHead(403, {});
                        res.end("RESULT: KO - User already exist") ;
                      } else {
                         mongoHandler.collection('Utenti').insert({username: jsResult.username, password: jsResult.password}, function(err, result) {
                             if(err != null) {
                               res.writeHead(500, {});
                               res.end("RESULT: KO") ;
                             } else {
                               res.end("OK");
                             }
                         });
                      }
                   }
            });
        });
    },


    InserisciIngressoUscita_Log: function(req, res, mongoHandler) {
        data = "";

        logger.log("[Gestione Ingressi] INGRESSI-USCITE [Inserimento]")

        req.on('data', function (chunk) {
           if(chunk !== undefined) {
             data += chunk;
           }
        }).on('end', function() {
         var jsResult;

         try {
           jsResult = JSON.parse(data);
         } catch (err) {
           res.writeHead(500, {});
           res.end("RESULT: KO -" + err) ;
           logger.log(err);
           return;
         }

         delete jsResult.password;

         mongoHandler.collection('IngressiUscite').findOne({username: jsResult.username, Time: {$regex : jsResult.Time.substring(0, 10) + ".*"}, Colonna: jsResult.Colonna }, function(err, result) {

           if(result == null && err == null)
           {
              // Inserire il log
              mongoHandler.collection('IngressiUscite').insert(jsResult, function(err, result) {
                    if(err != null) {
                      res.writeHead(500, {});
                      res.end("RESULT: KO") ;
                    } else {
                      logger.log("[Gestione Ingressi] Record inserito all'interno del DB");
                      res.end("OK");
                    }

                    return;
              });
              logger.log("[Gestione Ingressi] Record non presente");
              res.end("OK") ;
              return;
            }
            else
            {
                mongoHandler.collection('IngressiUscite').updateOne({ _id: result._id }, jsResult, function(err, result) {
                    if(err != null) {
                      res.writeHead(500, {});
                      res.end("RESULT: KO") ;
                    } else {
                      res.end("OK");
                    }
                });

                logger.log("[Gestione Ingressi] Record già presente all'interno del DB");
                res.end("OK") ;
                //res.writeHead(500, {});
                //res.end("RESULT: KO - Record già presente all'interno del DB") ;
                return;
            }
         });



       });

    },

    ModificaIngressoUscita_Log: function(req, res, mongoHandler) {
          data = "";

          logger.log("[Gestione Ingressi] INGRESSI-USCITE [Modifica]")

          req.on('data', function (chunk) {
             if(chunk !== undefined) {
               data += chunk;
             }
         }).on('end', function() {
           var jsResult;

           try {
             jsResult = JSON.parse(data);
           } catch (err) {
             res.writeHead(500, {});
             res.end("RESULT: KO -" + err) ;
             logger.log(err);
             return;
           }

           delete jsResult.password;

           mongoHandler.collection('IngressiUscite').findOne({username: jsResult.username, Time: jsResult.Time }, function(err, result) {

                if(result == null && err == null)
                {
                      logger.log("[Gestione Ingressi] Nessun ingresso o uscita presnte");
                      res.writeHead(500, {});
                      res.end("RESULT: KO - Record non presnte all'interno del DB") ;
                      return;
                }
                else
                {
                      mongoHandler.collection('IngressiUscite').updateOne({ _id: result._id }, jsResult, function(err, result) {
                          if(err != null) {
                            res.writeHead(500, {});
                            res.end("RESULT: KO") ;
                          } else {
                            res.end("OK");
                          }
                      });
                }
           });



         });
    },

    EliminaIngressoUscita_Log: function(req, res, mongoHandler) {
          data = "";

          logger.log("[Gestione Ingressi] INGRESSI-USCITE [Eliminazione]")

          req.on('data', function (chunk) {
             if(chunk !== undefined) {
               data += chunk;
             }
         }).on('end', function() {
           var jsResult;

           try {
             jsResult = JSON.parse(data);
           } catch (err) {
             res.writeHead(500, {});
             res.end("RESULT: KO -" + err) ;
             logger.log(err);
             return;
           }

           mongoHandler.collection('IngressiUscite').findOne({username: jsResult.username, Time: jsResult.Time, Movimento: jsResult.Movimento }, function(err, result) {

                if(result == null && err == null)
                {
                    logger.log("[Gestione Ingressi] Nessun ingresso o uscita presnte");
                    res.writeHead(500, {});
                    res.end("RESULT: KO - Record non presnte all'interno del DB") ;
                    return;
                }
                else
                {
                      mongoHandler.collection('IngressiUscite').deleteOne({ _id: result._id }, function(err, result) {
                          if(err != null) {
                            res.writeHead(500, {});
                            res.end("RESULT: KO") ;
                          } else {
                            res.end("OK");
                          }
                      });
                }
           });

         });
    },


    EliminaIngressoUscita_Giorno_Log: function (req, res, mongoHandler) {
        data = "";

        logger.log("[Gestione Ingressi] INGRESSI-USCITE [Eliminazione]")

        req.on('data', function (chunk) {
            if (chunk !== undefined) {
                data += chunk;
            }
        }).on('end', function () {
            var jsResult;

            try {
                jsResult = JSON.parse(data);
            } catch (err) {
                res.writeHead(500, {});
                res.end("RESULT: KO -" + err);
                logger.log(err);
                return;
            }

            mongoHandler.collection('IngressiUscite').findOne({ username: jsResult.username, Time: jsResult.Time }, function (err, result) {

                if (result == null && err == null) {
                    logger.log("[Gestione Ingressi] Nessun ingresso o uscita presnte");
                    res.writeHead(500, {});
                    res.end("RESULT: KO - Record non presnte all'interno del DB");
                    return;
                }
                else {
                    mongoHandler.collection('IngressiUscite').deleteOne({ username: jsResult.username, Time: jsResult.Time }, function (err, result) {
                        if (err != null) {
                            res.writeHead(500, {});
                            res.end("RESULT: KO");
                        } else {
                            res.end("OK");
                        }
                    });
                }
            });

        });
    },

    GetUltimoIngressoUscita_Log:function(req, res, mongoHandler) {
      logger.log("[Gestione Ingressi] INGRESSI-USCITE [Get Ultimo]")
      data = '';

      req.on('data', function (chunk) {
         if(chunk !== undefined) {
           data += chunk;
         }
      }).on('end', function() {
       var jsResult;

       try {
         jsResult = JSON.parse(data);
       } catch (err) {
         res.writeHead(500, {});
         res.end("RESULT: KO -" + err) ;
         logger.log(err);
         return;
       }

       mongoHandler.collection('IngressiUscite').find({username: jsResult.username}).sort({Time: -1, Colonna: -1}).limit(1).toArray(function(err, result) {
                  res.end(JSON.stringify(result));
          });
      });
    },

    GetAllIngressoUscita_Log: function(req, res, mongoHandler) {
        logger.log("[Gestione Ingressi] INGRESSI-USCITE [Lista]")
        data = '';

        req.on('data', function (chunk) {
           if(chunk !== undefined) {
             data += chunk;
           }
        }).on('end', function() {
         var jsResult;

         try {
           jsResult = JSON.parse(data);
         } catch (err) {
           res.writeHead(500, {});
           res.end("RESULT: KO -" + err) ;
           logger.log(err);
           return;
         }

         mongoHandler.collection('IngressiUscite').find({username: jsResult.username}).toArray(function(err, result) {
                    var output = {};
                    output.Ingressi = result;
                    mongoHandler.collection('Eventi').find({username: jsResult.username}).toArray(function(err, result) {
                      output.Eventi = result;
                      res.end(JSON.stringify(output));
                  });
            });
        });
    },

    InserisciEvento_Log: function(req, res, mongoHandler) {
        data = "";

        logger.log("[Gestione Ingressi] EVENTI [Inserimento]")

        //public TipoEvento Evento { get; set; }
        //public int NOreDaScalare { get; set; }

        req.on('data', function (chunk) {
           if(chunk !== undefined) {
             data += chunk;
           }
        }).on('end', function() {
         var jsResult;

         try {
           jsResult = JSON.parse(data);
         } catch (err) {
           res.writeHead(500, {});
           res.end("RESULT: KO -" + err) ;
           logger.log(err);
           return;
         }

         delete jsResult.password;

         mongoHandler.collection('Eventi').findOne({username: jsResult.username, Time: jsResult.Time }, function(err, result) {

                if(result == null && err == null)
                {
                  // Inserire il log
                  mongoHandler.collection('Eventi').insert(jsResult, function(err, result) {
                        if(err != null) {
                          res.writeHead(500, {});
                          res.end("RESULT: KO") ;
                        } else {
                          res.end("OK");
                        }

                        return;
                  });
              }
              else
              {
                  mongoHandler.collection('Eventi').updateOne({ _id: result._id }, jsResult, function(err, result) {
                      if(err != null) {
                        res.writeHead(500, {});
                        res.end("RESULT: KO") ;
                      } else {
                        res.end("OK");
                      }
                  });

                  logger.log("[Gestione Ingressi] E' già presente un evento per questa data" );                  
                  return;
              }
         });



       });
    },

    ModificaEvento_Log: function(req, res, mongoHandler) {
          data = "";

          logger.log("[Gestione Ingressi] EVENTI [Modifica]")

          req.on('data', function (chunk) {
             if(chunk !== undefined) {
               data += chunk;
             }
         }).on('end', function() {
           var jsResult;

           try {
             jsResult = JSON.parse(data);
           } catch (err) {
             res.writeHead(500, {});
             res.end("RESULT: KO -" + err) ;
             logger.log(err);
             return;
           }

           delete jsResult.password;

           mongoHandler.collection('Eventi').findOne({username: jsResult.username, Time: jsResult.Time }, function(err, result) {

                if(result == null && err == null)
                {
                    logger.log("[Gestione Ingressi] Record non presente all'interno del DB");
                    res.writeHead(500, {});
                    res.end("RESULT: KO - Record non presente all'interno del DB") ;
                    return;
                }
                else
                {
                    mongoHandler.collection('Eventi').updateOne({ _id: result._id }, jsResult, function(err, result) {
                        if(err != null) {
                          res.writeHead(500, {});
                          res.end("RESULT: KO") ;
                        } else {
                          res.end("OK");
                        }
                    });
                }
           });



         });
    },

    EliminaEvento_Log: function(req, res, mongoHandler) {
          data = "";

          logger.log("[Gestione Ingressi] EVENTI [Eliminazione]")

          req.on('data', function (chunk) {
             if(chunk !== undefined) {
               data += chunk;
             }
         }).on('end', function() {
           var jsResult;

           try {
             jsResult = JSON.parse(data);
           } catch (err) {
             res.writeHead(500, {});
             res.end("RESULT: KO -" + err) ;
             logger.log(err);
             return;
           }


           mongoHandler.collection('Eventi').findOne({username: jsResult.username, Time: jsResult.Time }, function(err, result) {

                if(result == null && err == null)
                {
                    logger.log("[Gestione Ingressi] Record non presente all'interno del DB");
                    res.writeHead(500, {});
                    res.end("RESULT: KO - Record non presente all'interno del DB") ;
                    return;
                }
                else
                {
                    mongoHandler.collection('Eventi').deleteOne({username: jsResult.username, Time: jsResult.Time }, function(err, result) {
                        if(err != null) {
                          res.writeHead(500, {});
                          res.end("RESULT: KO") ;
                        } else {
                          res.end("OK");
                        }
                    });
                }
           });



         });
    }



}
