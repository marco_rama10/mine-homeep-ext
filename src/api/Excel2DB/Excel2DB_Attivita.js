var logger = require("./../../logger.js");
var data = '';


module.exports = {

  InserisciAttivita_Log: function(req, res, mongoHandler) {
    data = "";

    logger.log("[Attivita] [Inserimento]");

    req.on('data', function (chunk) {
         if(chunk !== undefined) {
           data += chunk;
         }
    }).on('end', function() {
         var jsResult;

         try {
           jsResult = JSON.parse(data);
         } catch (err) {
           res.writeHead(500, {});
           res.end("RESULT: KO -" + err) ;
           logger.log(err);
           return;
         }


       mongoHandler.collection('Attivita').findOne({username: jsResult.username, Time: jsResult.Time, "Attivita.TipoAttivita": jsResult.Attivita.TipoAttivita, "Attivita.GiornoPalestra": jsResult.Attivita.GiornoPalestra }, function(err, result) {
         if(result == null && err == null) {
             mongoHandler.collection('Attivita').insert(jsResult, function(err, result) {
                   if(err != null) {
                     res.writeHead(500, {});
                     res.end("RESULT: KO") ;
                   } else {
                     logger.log("[Attivita] Record inserito all'interno del DB");
                     res.end("OK");
                   }

                   return;
             });
         }
         else {
             logger.log("[Attivita] Record già presente all'interno del DB");
             res.writeHead(500, {});
             res.end("RESULT: KO - Record già presente all'interno del DB") ;
             return;
         }
       });
     });


  },

  EliminaAttivita_Log: function(req, res, mongoHandler) {
    data = "";

    logger.log("[Attivita] [Eliminazione]")

    req.on('data', function (chunk) {
         if(chunk !== undefined) {
           data += chunk;
         }
    }).on('end', function() {
       var jsResult;

       try {
         jsResult = JSON.parse(data);
       } catch (err) {
         res.writeHead(500, {});
         res.end("RESULT: KO -" + err) ;
         logger.log(err);
         return;
       }

       mongoHandler.collection('Attivita').findOne({username: jsResult.username, Time: jsResult.Time, Tipo: jsResult.Tipo, Tipo2: jsResult.Tipo2 }, function(err, result) {
         if(result == null && err == null) {
             logger.log("[Attivita] Record non presente all'interno del DB");
             res.writeHead(500, {});
             res.end("RESULT: KO - Record non presente all'interno del DB") ;
             return;
         }
         else {
             mongoHandler.collection('Attivita').deleteOne({username: jsResult.username, Time: jsResult.Time, Tipo: jsResult.Tipo, Tipo2: jsResult.Tipo2 }, function(err, result) {
                 if(err != null) {
                   res.writeHead(500, {});
                   res.end("RESULT: KO") ;
                 } else {
                   res.end("OK");
                 }
             });
         }
       });
     });

  },

}
