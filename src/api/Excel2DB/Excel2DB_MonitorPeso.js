var logger = require("./../../logger.js");
var data = '';

module.exports = {

  InserisciPeso_Log: function(req, res, mongoHandler) {
    data = "";

    logger.log("[Monitor peso] [Inserimento]");

    req.on('data', function (chunk) {
         if(chunk !== undefined) {
           data += chunk;
         }
    }).on('end', function() {
         var jsResult;

         try {
           jsResult = JSON.parse(data);
         } catch (err) {
           res.writeHead(500, {});
           res.end("RESULT: KO -" + err) ;
           logger.log(err);
           return;
         }


       mongoHandler.collection('Peso').findOne({username: jsResult.username, Time: jsResult.Time }, function(err, result) {
         if(result == null && err == null) {
             mongoHandler.collection('Peso').insert(jsResult, function(err, result) {
                   if(err != null) {
                     res.writeHead(500, {});
                     res.end("RESULT: KO") ;
                   } else {
                     logger.log("[Monito peso] Record inserito all'interno del DB");
                     res.end("OK");
                   }

                   return;
             });
         }
         else {
             logger.log("[Monitor peso] Record già presente all'interno del DB");
             res.writeHead(500, {});
             res.end("RESULT: KO - Record già presente all'interno del DB") ;
             return;
         }
       });
    });


  },

  ModificaPeso_Log: function(req, res, mongoHandler) {
    data = "";

    logger.log("[Monitor peso] [Modifica]")

    req.on('data', function (chunk) {
         if(chunk !== undefined) {
           data += chunk;
         }

         var jsResult;

         try {
           jsResult = JSON.parse(data);
         } catch (err) {
           res.writeHead(500, {});
           res.end("RESULT: KO -" + err) ;
           logger.log(err);
           return;
         } jsResult = JSON.parse(data);

       mongoHandler.collection('Peso').findOne({username: jsResult.username, Time: jsResult.Time }, function(err, result) {
         if(result == null && err == null) {
             logger.log("[Monitor peso] Record non presente all'interno del DB");
             res.writeHead(500, {});
             res.end("RESULT: KO - Record non presente all'interno del DB") ;
             return;
         }
         else {
             mongoHandler.collection('Peso').updateOne({ _id: result._id }, jsResult, function(err, result) {
                 if(err != null) {
                   res.writeHead(500, {});
                   res.end("RESULT: KO") ;
                 } else {
                   res.end("OK");
                 }
             });
         }
       });
     });


  },

  EliminaPeso_Log: function(req, res, mongoHandler) {
    data = "";

    logger.log("[Monitor peso] [Eliminazione]")

    req.on('data', function (chunk) {
         if(chunk !== undefined) {
           data += chunk;
         }

       var jsResult;

       try {
         jsResult = JSON.parse(data);
       } catch (err) {
         res.writeHead(500, {});
         res.end("RESULT: KO -" + err) ;
         logger.log(err);
         return;
       }

       mongoHandler.collection('Peso').findOne({username: jsResult.username, Time: jsResult.Time }, function(err, result) {
         if(result == null && err == null) {
             logger.log("[Monitor peso] Record non presente all'interno del DB");
             res.writeHead(500, {});
             res.end("RESULT: KO - Record non presente all'interno del DB") ;
             return;
         }
         else {
             mongoHandler.collection('Peso').deleteOne({username: jsResult.username, Time: jsResult.Time }, function(err, result) {
                 if(err != null) {
                   res.writeHead(500, {});
                   res.end("RESULT: KO") ;
                 } else {
                   res.end("OK");
                 }
             });
         }
       });
     });

  },

}
