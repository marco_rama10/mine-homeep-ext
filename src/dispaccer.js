var logger = require("./logger.js");
//var mediat = require("./api/mediat.js");
//var film = require("./api/film.js");
//var secmediat = require("./api/secmediat.js");
//var privsection = require("./api/private.js");
//var sup3rV = require("./api/sup3rV.js");
//var Hook = require("./api/hook.js");
var Excel2DB_IngressiUscite = require("./api/Excel2DB/Excel2DB_IngressiUscite.js");
var Excel2DB_MonitorPeso = require("./api/Excel2DB/Excel2DB_MonitorPeso.js");
var Excel2DB_Attivita = require("./api/Excel2DB/Excel2DB_Attivita.js");
var Excel2DB_Spese = require("./api/Excel2DB/Excel2DB_Spese.js");


module.exports = {


   initializeRoute: function(app, mongoHandler) {
       // Add headers
       app.use(function (req, res, next) {

          // Website you wish to allow to connect
          res.setHeader('Access-Control-Allow-Origin', '*');
          // Request methods you wish to allow
          res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
          // Request headers you wish to allow
          res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
          // Set to true if you need the website to include cookies in the requests sent
          // to the API (e.g. in case you use sessions)
          res.setHeader('Access-Control-Allow-Credentials', true);

          // Pass to next layer of middleware
          next();
       });

       // ROOT
       app.get('/', function (req, res) {
         try {
           logger.log("[IsAlive] Ok!");
           res.end("Hello! I'm root and I'm alive!");
         } catch (err) {
           logger.log(err);
         }

       });

       // IS ALIVE
       app.get('/isAlive', function (req, res) {
         try {
           logger.log("[IsAlive] Ok!");
           res.end("Hello! I'm alive!");
         } catch (err) {
           logger.log(err);
         }

       });


   },

/*
   initializeRouteMediaT: function(app, mongoHandler) {

      // Media T (software)
      app.post('/mediat', function (req, res) {
        try {
          mediat.root(req, res, mongoHandler);
        } catch (err) {
          logger.log(err);
        }

      });

      app.post('/mediat/usdownload/start', function (req, res) {
        try {
          mediat.userdownload_start(req, res, mongoHandler);
        } catch (err) {
          logger.log(err);
        }

      });

      app.post('/mediat/usdownload/end', function (req, res) {
        try {
          mediat.userdownload_end(req, res, mongoHandler);
        } catch (err) {
          logger.log(err);
        }

      });

      app.post('/mediat/usdownload/cancel', function (req, res) {
        try {
          mediat.userdownload_cancel(req, res, mongoHandler);
        } catch (err) {
          logger.log(err);
        }

      });

      app.post('/mediat/uson', function (req, res) {
        try {
          mediat.useron(req, res, mongoHandler);
        } catch (err) {
          logger.log(err);
        }

      });

      app.post('/mediat/update', function (req, res) {
        try {
          mediat.update(req, res, mongoHandler);
        } catch (err) {
          logger.log(err);
        }

      });

      app.all('/mediat/updatefilename/:version/:filenumber', function (req, res) {
        try {
          mediat.downUpdateFileName(req, res, mongoHandler, req.params.version, req.params.filenumber);
        } catch (err) {
          logger.log(err);
        }

      });

      app.all('/mediat/download/:version/:filename', function (req, res) {
        try {
          mediat.downUpdateFile(req, res, mongoHandler, req.params.version, req.params.filename);
        } catch (err) {
          logger.log(err);
        }

      });


      // Media T (Web UI)
      app.get('/secmediat/', function (req, res) {
        try {
          secmediat.root(req, res, mongoHandler);
        } catch (err) {
          logger.log(err);
        }

      });

      app.get('/secmediat/users', function (req, res) {
        try {
          secmediat.users(req, res, mongoHandler);
        } catch (err) {
          logger.log(err);
        }

      });

      app.get('/secmediat/ustate', function (req, res) {
        try {
          secmediat.ustate(req, res, mongoHandler);
        } catch (err) {
          logger.log(err);
        }

      });

      app.get('/secmediat/usessions/:user', function (req, res) {
        try {
          secmediat.usessions(req, res, req.params.user, mongoHandler);
        } catch (err) {
          logger.log(err);
        }

      });

      app.get('/secmediat/ulastsession/:user', function (req, res) {
        try {
          secmediat.ulastsession(req, res, req.params.user, mongoHandler);
        } catch (err) {
          logger.log(err);
        }

      });
  },

  initializeRouteFilm: function(app, mongoHandler) {

      // Film
      app.post('/film/saving', function (req, res) {
        try {
          film.saving(req, res, mongoHandler);
        } catch (err) {
          logger.log(err);
        }
      });


      app.get('/film/saved/:profile', function (req, res) {
        try {
          film.getSaved(req, res, req.params.profile, mongoHandler);
        } catch (err) {
          logger.log(err);
        }
      });

      app.get('/film/profiles', function (req, res) {
        try {
          film.getProfiles(req, res, mongoHandler);
        } catch (err) {
          logger.log(err);
        }
      });

      app.get('/film/remove/:profile', function (req, res) {
        try {
          film.removeProfile(req, res, req.params.profile, mongoHandler);
        } catch (err) {
          logger.log(err);
        }
      });

      app.post('/film/getComingSoon/', function (req, res) {
        try {
          film.getComingSoon(req, res, mongoHandler);
        } catch (err) {
          logger.log(err);
        }
      });

   },

   initializeRoutePrivate: function(app) {
        app.get('/private/insert/:pin', function(req, res) {
          try {
            privsection.insertPin(req, res);
          } catch (err) {
            logger.log(err);
          }
        });

        app.get('/private/getSections/:pass', function(req, res) {
          try {
            privsection.getSections(req, res);
          } catch (err) {
            logger.log(err);
          }
        });
   },

   initializeSup3rV: function(app, mongoHandler) {

         app.post('/sup3rV/iamyourslave/:username', function(req, res) {
           try {
             sup3rV.iamyourslave(req, res, mongoHandler);
           } catch (err) {
             logger.log(err);
           }
         });

        app.get('/sup3rV/getsoftware/:username', function(req, res) {
          try {
            sup3rV.getsoftware(req, res, mongoHandler);
          } catch (err) {
            logger.log(err);
          }
        });

        app.get('/sup3rV/getmodule/:username/:module/:version', function(req, res) {
          try {
            sup3rV.getmodule(req, res, mongoHandler);
          } catch (err) {
            logger.log(err);
          }
        });
   },

   initializeHook:function(app, mongoHandler) {
       app.post('/Hook/event/:username', function(req, res) {
         try {
           Hook.registerEvent(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Hook/request', function(req, res) {
         try {
           Hook.requestEvent(req, res);
         } catch (err) {
           logger.log(err);
         }
       });

   },
*/
   initializeExcel2DB: function(app, mongoHandler) {
       app.post('/Excel2DB/GestioneIngressi/Login', function(req, res) {
         try {
           Excel2DB_IngressiUscite.Login(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/GestioneIngressi/Register', function(req, res) {
         try {
           Excel2DB_IngressiUscite.Register(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });


       app.post('/Excel2DB/GestioneIngressi/Post', function(req, res) {
         try {
           Excel2DB_IngressiUscite.InserisciIngressoUscita_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/GestioneIngressi/Put', function(req, res) {
         try {
           Excel2DB_IngressiUscite.ModificaIngressoUscita_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/GestioneIngressi/Delete', function(req, res) {
         try {
           Excel2DB_IngressiUscite.EliminaIngressoUscita_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/GestioneIngressi/DeleteDay', function (req, res) {
           try {
               Excel2DB_IngressiUscite.EliminaIngressoUscita_Giorno_Log(req, res, mongoHandler);
           } catch (err) {
               logger.log(err);
           }
       });

       app.post('/Excel2DB/GestioneIngressi/GetAll', function(req, res) {
         try {
           Excel2DB_IngressiUscite.GetAllIngressoUscita_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/GestioneIngressi/GetLast', function(req, res) {
         try {
           Excel2DB_IngressiUscite.GetUltimoIngressoUscita_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/GestioneEventi/Post', function(req, res) {
         try {
           Excel2DB_IngressiUscite.InserisciEvento_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/GestioneEventi/Put', function(req, res) {
         try {
           Excel2DB_IngressiUscite.ModificaEvento_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/GestioneEventi/Delete', function(req, res) {
         try {
           Excel2DB_IngressiUscite.EliminaEvento_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/MonitorPeso/Post', function(req, res) {
         try {
           Excel2DB_MonitorPeso.InserisciPeso_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/MonitorPeso/Put', function(req, res) {
         try {
           Excel2DB_MonitorPeso.ModificaPeso_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/MonitorPeso/Delete', function(req, res) {
         try {
           Excel2DB_MonitorPeso.EliminaPeso_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/Attivita/Post', function(req, res) {
         try {
           Excel2DB_Attivita.InserisciAttivita_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/Attivita/Delete', function(req, res) {
         try {
           Excel2DB_Attivita.EliminaAttivita_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/Spese/Post', function(req, res) {
         try {
           Excel2DB_Spese.InserisciSpesa_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/Spese/Delete', function(req, res) {
         try {
           Excel2DB_Spese.EliminaSpesa_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/Mensilita/Post', function(req, res) {
         try {
           Excel2DB_Spese.InserisciMensilita_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });

       app.post('/Excel2DB/Mensilita/Delete', function(req, res) {
         try {
           Excel2DB_Spese.EliminaMensilita_Log(req, res, mongoHandler);
         } catch (err) {
           logger.log(err);
         }
       });
   }

};
