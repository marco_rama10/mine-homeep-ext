var nodemailer = require('nodemailer');
var logger = require("./logger.js");

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    /*type: 'OAuth2',
    clientId: 'clientid',
    clientSecret: 'clientsecret',
    refreshToken: 'refreshtoken',
    accessToken: 'accesstoken',
    expires: 12345,*/
    user: 'noreply.HomeEP@gmail.com',
    pass: 'xQij62Nn86cSjC9mwWid'
  }
});

var mailTest = {
  from: 'Home EP Communicator <noreply.HomeEP@gmail.com>',
  to: 'marco.doria85@gmail.com',
  subject: 'HomeEP Server (TEST)',
  text: 'Test MAIL'
};


var mailInfo = {
  from: 'Home EP Communicator <noreply.HomeEP@gmail.com>',
  to: 'marco.doria85@gmail.com'
};

module.exports = {


   sendTestMail: function() {

     transporter.sendMail(mailTest, function(error, info){
       if (error) {
         logger.log(error);
       } else {
         logger.log('Email sent: ' + info.response);
       }
      });
   },


   sendMail: function(title, message) {

     mailInfo.subject = 'HomeEP Server (' + title + ')';
     mailInfo.text = message;

     transporter.sendMail(mailInfo, function(error, info){
       if (error) {
         logger.log(error);
       } else {
         logger.log('Email sent: ' + info.response);
       }
      });
   }

};
