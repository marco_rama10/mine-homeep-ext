var http = require('http');
var express = require('express');
var url = require('url');
var mongoClient = require('mongodb').MongoClient;
//var WebSocketServer = require('websocket').server;
////////////////////// MY MODULE //////////////////////
///////////////////////////////////////////////////////
var logger = require("./src/logger.js");
var dispaccer = require("./src/dispaccer.js");
var server = null;
//var wsServer = null;
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////             MONGO DB CONNECTION           ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
//var _mongoDBHandlerMediaT = null;
//var _mongoDBHandlerFilm = null;
//var _mongoDBHandlerSup3rV = null;
//var _mongoDBHandlerHook = null;
var _mongoDBHandlerExcel2DB = null;

function allDBareConnected() {

  if(
    //_mongoDBHandlerMediaT == null ||
    //_mongoDBHandlerFilm == null ||
    //_mongoDBHandlerSup3rV == null ||
    //_mongoDBHandlerHook == null ||
    _mongoDBHandlerExcel2DB == null) {
       return false;
     }

     return true;
}

//mongoClient.connect('mongodb://127.0.0.1:27017/media_t', function(error, clientHandler) {
//    if(!error) {
//       logger.log("Connected do DB (MediaT)");
//       _mongoDBHandlerMediaT = clientHandler;

//       if(allDBareConnected()) {
//            WebServiceInitialize(function() {
//              logger.log("Initializing Web Socket");
//              initializeWebSocket();
//           });
//       }
//    }
//    else {
//       logger.log("Error during connection to DB");
//    }

//});


//mongoClient.connect('mongodb://127.0.0.1:27017/film', function(error, clientHandler) {
//    if(!error) {
//       logger.log("Connected do DB (Film)");
//       _mongoDBHandlerFilm = clientHandler;

//       if(allDBareConnected()) {
//            WebServiceInitialize(function() {
//              logger.log("Initializing Web Socket");
//              initializeWebSocket();
//           });
//       }
//    }
//    else {
//       logger.log("Error during connection to DB");
//    }

//});


//mongoClient.connect('mongodb://127.0.0.1:27017/sup3rv', function(error, clientHandler) {
//    if(!error) {
//       logger.log("Connected do DB (Sup3rV)");
//       _mongoDBHandlerSup3rV = clientHandler;

//       if(allDBareConnected()) {
//            WebServiceInitialize(function() {
//              logger.log("Initializing Web Socket");
//              initializeWebSocket();
//           });
//       }
//    }
//    else {
//       logger.log("Error during connection to DB");
//    }

//});


//mongoClient.connect('mongodb://127.0.0.1:27017/Hook', function(error, clientHandler) {
//    if(!error) {
//       logger.log("Connected do DB (Hook)");
//       _mongoDBHandlerHook = clientHandler;

//       if(allDBareConnected()) {
//            WebServiceInitialize(function() {
//              logger.log("Initializing Web Socket");
//              initializeWebSocket();
//           });
//       }
//    }
//    else {
//       logger.log("Error during connection to DB");
//    }

//});


//mongoClient.connect('mongodb://127.0.0.1:27017/Excel2DB', function(error, clientHandler) {
mongoClient.connect('mongodb://rama10:marmotta73!@ds113923.mlab.com:13923/excel2db', function(error, clientHandler) {
    if(!error) {
       logger.log("Connected do DB (Excel2DB)");
       _mongoDBHandlerExcel2DB = clientHandler;

       if(allDBareConnected()) {
            WebServiceInitialize(function() {
              logger.log("Initializing Web Socket");
              //initializeWebSocket();
           });
       }
    }
    else {
       logger.log("Error during connection to DB");
    }

});


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////             SERVER INITIALIZE             ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
var app = express();

function WebServiceInitialize(success_cb)
{
  dispaccer.initializeRoute(app);
  //dispaccer.initializeRouteMediaT(app, _mongoDBHandlerMediaT);
  //dispaccer.initializeRouteFilm(app, _mongoDBHandlerFilm);
  //dispaccer.initializeRoutePrivate(app);
  //dispaccer.initializeSup3rV(app, _mongoDBHandlerSup3rV);
  //dispaccer.initializeHook(app, _mongoDBHandlerHook);
  dispaccer.initializeExcel2DB(app, _mongoDBHandlerExcel2DB);
  var port = 8080;
  server = http.createServer(app).listen(port, function (req, res) {
      logger.log('Server Created! (Port:'+ port + ')');
      success_cb();
  });
}

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////             WEB SOCKET INITIALIZE             ///////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
/*
var connectionsSite = [];
var connectionsHook = [];
var connectionsHookWebInterface = [];




function originIsAllowed(origin) {
  return true;
}

function countConnections() {
  var count = 0;

  for(var i in connectionsSite) {
    count++;
  }

  return count;
}

function getId() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

function initializeWebSocket() {

  wsServer = new WebSocketServer({
      httpServer: server,
      autoAcceptConnections: false
  });



  wsServer.on('request', function(request) {
      if (!originIsAllowed(request.origin)) {
        request.reject();
        logger.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
        return;
      }

      var connection = request.accept('echo-protocol', request.origin);

      connection.id = getId();

      var resource = request.resource;
          resource = resource.replace('/', '');
          resource = resource.replace('?', '');
          resource = resource.replace('namespace=', '');

      if(resource.toLowerCase() === 'site')  {
          connectionsSite[connection.id] = connection;
      }

      if(resource.toLowerCase() === 'hook')  {
          connectionsHook[connection.id] = connection;
      }

      if(resource.toLowerCase() === 'hookwebinterface')  {
          connectionsHookWebInterface[connection.id] = connection;
      }


      logger.log((new Date()) + ' Connection accepted (Connection ID: '+ connection.id + '). Online users: ' + countConnections());

      connection.on('message', function(message) {
          //if (message.type === 'utf8') {
          //    logger.log('Received Message: ' + message.utf8Data);
          //    connection.sendUTF(message.utf8Data);
          //}
          //else if (message.type === 'binary') {
          //    logger.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
          //    connection.sendBytes(message.binaryData);
          //}
      });
      connection.on('close', function(reasonCode, description) {
          if(typeof connectionsSite[connection.id] !== 'undefined') {
              delete connectionsSite[connection.id];
          }
          if(typeof connectionsHook[connection.id] !== 'undefined') {
              delete connectionsHook[connection.id];
          }
          if(typeof connectionsHookWebInterface[connection.id] !== 'undefined') {
              delete connectionsHookWebInterface[connection.id];
          }


          logger.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected. Online users: ' + countConnections());
      });
  });

  global.WSocketSite = {
    sendMessage: function(message) {
       var count = 0;
       for(var i in connectionsSite) {
            connectionsSite[i].send(message);
            count++;
       }
       logger.log("sent " + count + " messages");
    }

  };

  global.WSocketHook = {
    sendMessage: function(message) {
       var count = 0;
       for(var i in connectionsHook) {
            connectionsHook[i].send(message);
            count++;
       }
       logger.log("sent " + count + " messages");
    }

  };

  global.WSocketHookWebInterface = {
    sendMessage: function(message) {
       var count = 0;
       for(var i in connectionsHookWebInterface) {
            connectionsHookWebInterface[i].send(message);
            count++;
       }
       logger.log("sent " + count + " messages");
    }

  };
}
*/
