module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
	copy: {
            EP: {
                  files : [
                      { src: 'index.js', dest: '/var/www/HomeEP/index.js'},
                      { src: 'variables.js', dest: '/var/www/HomeEP/variables.js'},
                      { src: 'src/**', dest: '/var/www/HomeEP/'},
                      { src: 'download/**', dest: '/var/www/HomeEP/'},
                      { src: 'node_modules/**', dest: '/var/www/HomeEP/'}
                  ]
            },
        },


  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');


  grunt.registerTask('dist', ['copy:EP']);
  grunt.registerTask('default', ['dist']);

};
